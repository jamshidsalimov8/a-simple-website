import Vue from 'vue'
import Vuex from 'vuex'
import RigstrPro from './market/RigstrProduct'
import Table from "@/store/Table/Table.js"


Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    //for example======
    // api_key: "b08763deb4ab9028285878cfdbb46ad0",
    // url_base: "https://api.openweathermap.org/data/2.5/",
    // weather: {},
  },
  actions: {
    //for example=========
    // async fetch_weather(ctx, place){
    //   const response = await fetch("https://api.openweathermap.org/data/2.5/weather?q=" + place + "&units=metric&APPID=b08763deb4ab9028285878cfdbb46ad0")
    //   const data = await response.json()
    //   ctx.commit('updateWeather', data)
    // },
  
  },
  
  mutations: {
  //for example========

  // updateWeather(state, data){
  //   state.weather = data;
  // },

  // HavolaQushish(state,newName){
  //   state.havolalar.push(newName); 
  // },

  // HavolaniUchirish(state,havolaId){
  //   state.havolalar.splice(havolaId,1)
  // },
  },
  getters: {
    // for example=====
  //  options(state){
  //    return state.options
  //  }
  },
  modules: {
    // for example ===
    RigstrPro,
    Table,
  }
})
