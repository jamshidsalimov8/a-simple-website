import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/sobity',
    name: 'sobity',
    meta: {layout: 'main'},
    component: () => import(/* webpackChunkName: "about" */ '@/views/Sobity.vue')
  },
  {
    path: '/users',
    name: 'users',
    meta: {layout: 'main'},
    component: () => import(/* webpackChunkName: "about" */ '@/views/Users.vue')
  },
  {
    path: '/shablon',
    name: 'shablon',
    meta: {layout: 'main'},
    component: () => import(/* webpackChunkName: "about" */ '@/views/Shablon.vue')
  },
  {
    path: '/settings',
    name: 'settings',
    meta: {layout: 'main'},
    component: () => import(/* webpackChunkName: "about" */ '@/views/Settings.vue')
  },
  {
    path: '/',
    name: 'login',
    meta: {layout: 'login'},
    component: () => import(/* webpackChunkName: "about" */ '@/views/LoginCart.vue')
  },
  {
    path: '/modalform/:id',
    name: 'modalform',
    meta: {layout: 'main'},
    component: () => import(/* webpackChunkName: "about" */ '@/components/BootstrapComp/ModalForm.vue')
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

// router.beforeEach((to, form, next)=>{
//   if(to.path != '/login'){
//     if(localStorage.Login != ''){
//       next()
//     }
//     else{
//       next('/login')
//     }
//   }
//   else{
//     next()
//   }
// })

export default router
